package com.osotnikov.samples.sainsbury.html.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.osotnikov.samples.sainsbury.SainsburyScraper;
import com.osotnikov.samples.sainsbury.spring.configuration.SainsburyConfiguration;
import com.osotnikov.utils.html.parser.jsoup.JsoupParserUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SainsburyConfiguration.class})
@SpringBootTest
public class SainsburyHtmlParserServiceIT {

	@Autowired
	private SainsburyHtmlParserService parserService;
	
	@Autowired
	private JsoupParserUtils parserUtils;

	@Test
	public void loadDocumentFromUrlSuccess() throws IOException {

		Document doc = parserUtils.loadDocumentFromUrl(SainsburyScraper.URL_OF_PARSABLE);
		String docTitle = doc.head().getElementsByTag("title").text();

		assertEquals("Berries, cherries & currants | Sainsbury's", docTitle);
	}

	@Test
	public void parseBerriesCherriesCurrantsAsJsonSuccess() throws IOException {

		String listOfProductsWithTotal = parserService.parseBerriesCherriesCurrantsAsJson(SainsburyScraper.URL_OF_PARSABLE);
		
		String sampleProduct =
			"{\r\n" +
			"    \"title\" : \"Sainsbury's Redcurrants 150g\",\r\n" +
			"    \"description\" : \"by Sainsbury's redcurrants\",\r\n" +
			"    \"kcal_per_100g\" : null,\r\n" +
			"    \"unit_price\" : \"2.50\"\r\n" +
			"  }";
		
		assertTrue(listOfProductsWithTotal.contains(sampleProduct));
	}


}
