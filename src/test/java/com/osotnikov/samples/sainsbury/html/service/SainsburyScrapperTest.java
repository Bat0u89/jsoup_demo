package com.osotnikov.samples.sainsbury.html.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.osotnikov.samples.sainsbury.SainsburyScraper;
import com.osotnikov.samples.sainsbury.feedback.service.UserFeedbackService;
import com.osotnikov.samples.sainsbury.spring.configuration.SainsburyConfiguration;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {SainsburyConfiguration.class})
@SpringBootTest
public class SainsburyScrapperTest {

	@Mock
	private SainsburyHtmlParserService parserService;
	
	@Mock
	private UserFeedbackService userFeedbackService;
	
	@InjectMocks
	private SainsburyScraper sainsburyScrapper;
	
	@Test
	public void success() throws Exception {
		
		String validResultFeedback = "imagine this is a valid result";
		
		when(parserService.parseBerriesCherriesCurrantsAsJson(SainsburyScraper.URL_OF_PARSABLE)).thenReturn(validResultFeedback);
		
		sainsburyScrapper.run(new String[0]);
		
		verify(userFeedbackService).displayFeedbackToUser(validResultFeedback);
		
	}
}
