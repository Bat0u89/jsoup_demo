package com.osotnikov.utils.html.parser.jsoup;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsoupParserUtils {

	public Document loadDocumentFromUrl(String url) throws IOException {

		log.debug("Called with url: {}", url);

		Document doc = Jsoup.connect(url).get();

		log.trace("\n\n DOCUMENT \n\n");
		log.trace("URL: {} \n\n", url);
		log.trace(doc.html());

		return doc;
	}

	/**
	 * Will parse the text content from the element under the baseElement that is found by applying cssElementSelector
	 * to the base element.
	 *
	 * @throws IllegalStateException if more than one matching elements were found using the cssElementSelector or no elements were found.
	 *
	 * */
	public String parseTextFromElement(Element baseElement, String cssElementSelector) {

		Elements discoveredElements = baseElement.select(cssElementSelector);
		if(discoveredElements.size() != 1) {

			throw new IllegalStateException(
				String.format("Found element in unparsable form, for cssElementSelector: %s",
					cssElementSelector));
		} else {
			return discoveredElements.first().text();
		}

	}
	
	/**
	 * Will parse the attribute value from the attribute with the name of attributeName of the element under the baseElement that is found
	 * by applying cssElementSelector to the base element.
	 *
	 * @throws IllegalStateException if more than one matching elements were found using the cssElementSelector or no elements were found.
	 *
	 * */
	public String parseAttributeValueFromElement(Element baseElement, String cssElementSelector, String attributeName) {

		Elements discoveredElements = baseElement.select(cssElementSelector);
		if(discoveredElements.size() != 1) {

			throw new IllegalStateException(
				String.format("Found element in unparsable form, for cssElementSelector: %s and attributeName %s",
					cssElementSelector, attributeName));
		} else {
			return discoveredElements.first().attr(attributeName);
		}

	}

}
