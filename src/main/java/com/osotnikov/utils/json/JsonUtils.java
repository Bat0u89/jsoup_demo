package com.osotnikov.utils.json;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class JsonUtils {
	
	public String formatJson(String unformattedJson) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		Object json = objectMapper.readValue(unformattedJson, Object.class);
		String indented = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		
		return indented;
	}
	
}
