package com.osotnikov.model;

import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class Price {

    @Size(min=0)
	Long units = 0l;

    @Size(min=0, max=100)
	int subunits = 0;

}
