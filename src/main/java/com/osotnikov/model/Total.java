package com.osotnikov.model;

import java.math.BigDecimal;
import java.util.Locale;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;

import org.javamoney.moneta.FastMoney;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Total {

	public final static float VAT = 0.2f;
	public final static CurrencyUnit CURRENCY = Monetary.getCurrency("GBP");
	public final static MonetaryAmountFormat AMOUNT_FORMAT = MonetaryFormats.getAmountFormat(AmountFormatQueryBuilder.
		of(Locale.UK).set("pattern", "0.00").build());
	
	public MonetaryAmount gross;
	
	public Total() {
		
		gross = FastMoney.of(new BigDecimal(0d), CURRENCY);
	}
	
	public String getGross() {

		String formattedGross = AMOUNT_FORMAT.format(gross);
		return formattedGross;
	}
	
	public void addToGross(BigDecimal amountToAdd) {
		MonetaryAmount monetaryAmountToAdd  = FastMoney.of(amountToAdd, CURRENCY);
		gross = gross.add(monetaryAmountToAdd);
	}
	
	@JsonProperty("vat")
	public String getVatOfGross() {
		
		if(gross == null) {
			return null;
		} else {
			
			MonetaryAmount vatOfGross = gross.multiply(VAT);
			String formattedVatOfGross = AMOUNT_FORMAT.format(vatOfGross);
			return formattedVatOfGross;
		}
		
	}

}
