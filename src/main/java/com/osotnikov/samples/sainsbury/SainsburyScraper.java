package com.osotnikov.samples.sainsbury;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.osotnikov.samples.sainsbury.feedback.service.UserFeedbackService;
import com.osotnikov.samples.sainsbury.html.service.SainsburyHtmlParserService;

import lombok.extern.slf4j.Slf4j;

/**
 * Entry point
 */
@SpringBootApplication
@Slf4j
public class SainsburyScraper implements CommandLineRunner {

	public static String URL_OF_PARSABLE = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
	
	@Autowired
	SainsburyHtmlParserService parserService; // Command line application run by single user so no concurrency issues to worry about.
	
	@Autowired
	UserFeedbackService userFeedbackService; // Command line application run by single user so no concurrency issues to worry about.

    public static void main(String[] args ) {

    	log.info("Greetings :)");
        SpringApplication.run(SainsburyScraper.class, args);
        log.info("Goodbye :)");
    }

	@Override
	public void run(String... args) throws Exception {

		// Display command line arguments
        for (int i = 0; i < args.length; ++i) {
        	log.info("command line argument [{}]: {}", i, args[i]);
        }

        if(args.length > 1) {
        	throw new IllegalArgumentException("This application is supposed to be called with no arguments or a single argument that is the Saisbury URL to parse.");
        } else if(args.length == 1) {
        	URL_OF_PARSABLE = args[0];
        }

        String formattedJson = parserService.parseBerriesCherriesCurrantsAsJson(URL_OF_PARSABLE);
        
        userFeedbackService.displayFeedbackToUser(formattedJson);
	}
}
