package com.osotnikov.samples.sainsbury.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.osotnikov.utils.html.parser.jsoup.JsoupParserUtils;

@Configuration
@ComponentScan(basePackages="com.osotnikov")
public class SainsburyConfiguration {

	@Bean
	public JsoupParserUtils getJsoupParserUtils() {
		return new JsoupParserUtils();
	}

}
