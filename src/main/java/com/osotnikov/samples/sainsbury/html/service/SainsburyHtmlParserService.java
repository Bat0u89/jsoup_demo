package com.osotnikov.samples.sainsbury.html.service;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.osotnikov.model.Total;
import com.osotnikov.samples.sainsbury.model.Product;
import com.osotnikov.samples.sainsbury.model.SainsburyScrapperResult;
import com.osotnikov.utils.html.parser.jsoup.JsoupParserUtils;
import com.osotnikov.utils.json.JsonUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SainsburyHtmlParserService {

	private final static String CSS_SELECTOR_FOR_PRODUCT = "div .product";
	private final static String CSS_SELECTOR_FOR_LINK_TO_PRODUCT = "div .productNameAndPromotions a";
	private final static String CSS_SELECTOR_FOR_ELEMENT_CONTAINING_THE_PRODUCT_NAME = CSS_SELECTOR_FOR_LINK_TO_PRODUCT;
	private final static String CSS_SELECTOR_FOR_PRODUCT_PRICE_PER_UNIT = "p.pricePerUnit";
	private final static String TEXT_AFTER_PRICE_PER_UNIT = "/unit";
	
	private final static String SELECTOR_FOR_POSSIBLE_KCAL_ELEMENT = ".nutritionTable td.nutritionLevel1";
	private final static String ENERGY_TEXT_RECOGNIZER_STRING = "kcal";
	
	private final static String SELECTOR_FOR_HEADER_BEFORE_DESCRIPTION_ELEMENT = "h3.productDataItemHeader";
	private final static String DESCRIPTION_HEADER_TEXT = "Description";

	@Autowired
	private JsoupParserUtils parserUtils;
	
	@Autowired
	JsonUtils jsonUtils;
	
	public String parseNutritionalEnergy(Document baseElement) {
		
		Elements discoveredElements = baseElement.select(SELECTOR_FOR_POSSIBLE_KCAL_ELEMENT);
		String energyElementText = null;
		
		for(Element energyElement : discoveredElements) {
			
			energyElementText = energyElement.text();
			
			if(StringUtils.containsIgnoreCase(energyElementText, ENERGY_TEXT_RECOGNIZER_STRING)) {
				return energyElementText;
			}
		}
		
		return null;
	}
	
	public String parseProductDescription(Document baseElement) {

		Elements discoveredElements = baseElement.select(SELECTOR_FOR_HEADER_BEFORE_DESCRIPTION_ELEMENT);
		String headerText = null;
		Element descriptionElement = null;
		Element firstDescriptionParagraph = null;
		String firstDescriptionParagraphText = null;
		
		for(Element possibleDescriptionHeaderElement : discoveredElements) {
			
			headerText = possibleDescriptionHeaderElement.text();
			
			if(StringUtils.containsIgnoreCase(headerText, DESCRIPTION_HEADER_TEXT)) {
				
				// Product description is contained within <p>s that are inside a div, the one next to
				// the header we extracted headerText from just now.
				// We care only about the text inside the first <p> element as per requirements.
				descriptionElement = possibleDescriptionHeaderElement.nextElementSibling();
				if(descriptionElement == null) {
					return null;
				}
				firstDescriptionParagraph = descriptionElement.child(0);
				
				if(firstDescriptionParagraph == null) {
					return null;
				}
				firstDescriptionParagraphText = firstDescriptionParagraph.text();
				
				return firstDescriptionParagraphText;
			}
		}
		
		return null;
	}

	public String parseBerriesCherriesCurrantsAsJson(String url) throws JsonGenerationException, JsonMappingException, IOException {

		Document productsPage = parserUtils.loadDocumentFromUrl(url);
		
		Elements productInfoEls = productsPage.select(CSS_SELECTOR_FOR_PRODUCT);
		Product product = null;
		
		SainsburyScrapperResult result = new SainsburyScrapperResult();
		List<Product> products = result.getResults();
		Total total = result.getTotal();
		
		for(Element productInfoEl : productInfoEls) {

			product = new Product();

			try {
				// title, will skip this product instance if not provided
				product.setTitle(
					parserUtils.parseTextFromElement(productInfoEl, CSS_SELECTOR_FOR_ELEMENT_CONTAINING_THE_PRODUCT_NAME));
				
				// price per unit, will skip this product instance if not provided
				String parsedPriceString =
					parserUtils.parseTextFromElement(productInfoEl, CSS_SELECTOR_FOR_PRODUCT_PRICE_PER_UNIT);
				parsedPriceString = parsedPriceString.replace("£", "");
				parsedPriceString = StringUtils.substringBefore(parsedPriceString, TEXT_AFTER_PRICE_PER_UNIT);
				product.setPricePerUnit(parsedPriceString);
				BigDecimal price = new BigDecimal(parsedPriceString);
				total.addToGross(price);
				
				// product page document, will skip this product instance if not provided
				String linkToProductPage =
						parserUtils.parseAttributeValueFromElement(productInfoEl, CSS_SELECTOR_FOR_LINK_TO_PRODUCT, "abs:href");
				Document productPageDocument = parserUtils.loadDocumentFromUrl(linkToProductPage);
				
				// energy in kcal, may be null
				product.setKcalPer100g(parseNutritionalEnergy(productPageDocument));
				
				// description, may be null
				product.setDescription(parseProductDescription(productPageDocument));
				
				products.add(product);
				
			} catch(IllegalStateException e) {
				// Don't include elements in non parsable format in the result.
				log.error(e.getMessage());
				continue;
			} catch (IOException e) {
				log.error(e.getMessage());
				continue;
			}

		}
		
		log.debug("\n\nScraped products: {}\n\n", products);
		
		ObjectMapper objectMapper = new ObjectMapper();
		StringWriter outputWriter = new StringWriter();
		objectMapper.writeValue(outputWriter, result);
		outputWriter.flush();
		outputWriter.close();
		
		String resultAsJson = outputWriter.toString();
		log.trace("\n\nScraped products as json: {}\n\n", outputWriter.toString());
		
		String resultAsFormattedJson = jsonUtils.formatJson(resultAsJson);
		log.debug("\n\nScraped products as indented json: {}\n\n", resultAsFormattedJson);

		return resultAsFormattedJson;
	}

}
