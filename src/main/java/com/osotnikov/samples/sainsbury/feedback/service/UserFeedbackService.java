package com.osotnikov.samples.sainsbury.feedback.service;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserFeedbackService {

	public void displayFeedbackToUser(String feedback) {
		log.info(feedback);
	}
	
}
