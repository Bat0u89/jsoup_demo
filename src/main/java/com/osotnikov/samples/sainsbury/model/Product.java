package com.osotnikov.samples.sainsbury.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Product {

	@NotNull
	private String title;
	@JsonProperty("kcal_per_100g")
	private String kcalPer100g;
	@NotNull
	@JsonProperty("unit_price")
    private String pricePerUnit;
    private String description;

}
