package com.osotnikov.samples.sainsbury.model;

import java.util.LinkedList;
import java.util.List;

import com.osotnikov.model.Total;

public class SainsburyScrapperResult {

	List<Product> results = new LinkedList<>();
	Total total = new Total();
	
	public List<Product> getResults() {
		return results;
	}
	public void setResults(List<Product> results) {
		this.results = results;
	}
	public Total getTotal() {
		return total;
	}
	public void setTotal(Total total) {
		this.total = total;
	}
	
}
