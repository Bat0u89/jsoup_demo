# Sainsbury's interview assignment

Frameworks Used
---

Spring Boot, Jsoup,  
JavaMoney (Moneta): see com.osotnikov.model.Total,  
Slf4j, Log4j,  
Junit, Mockito  

Tests
---

There is one unit test (SainsburyScrapperTest) and one integration test (SainsburyHtmlParserServiceIT). The tests are minimal
and are included solely for presenting a sample of my knowledge on testing. Due to personal time constraints I cannot
provide satisfactory test coverage.

SainsburyScrapperTest: Presents a simple scenario where we want to mock the return value of a dependency and want to
make sure that another dependency was called with the right arguments during the execution of the service method under
test. 

SainsburyHtmlParserServiceIT: This test is an integaration test of SainsburyHtmlParserService, instead of mocks the actual
dependencies are provided (SainsburyHtmlParserService, JsoupParserUtils), this is achieved by the appropriate spring
context configuration.

Surefire plugin is configure appropriately. This means that unit tests run during package and integration tests during 
instal.

How to start the RevolutInterviewAssignment application
---

1. Run `mvn clean install` or `mvn clean package` to build your application. Note that `mvn package spring-boot:repackage` 
is needed in case you want to create an executable uber jar.
1. Start application with `mvn spring-boot:run` from the repository directory or 
if you don't have maven installed you can run the executable with this command
`java -jar target/sainsbury.assignment-1.jar` (included only for thre reviewer's convenience)


